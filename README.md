SNMP
====
Role to install SNMPD and configure SNMPv2 

Requirements
------------
None

Role Variables
--------------

| Variable  | Type  |Default  | Descirption  |
|---|---|---|---|
|  snmpd_v2_snmpcommunity | String  | public  | SNMPv2 Community String  |
|  snmpd_v2_pollers | List |  | List of host that can poll host. defaults to any|
|  snmpd_open_ports |  boolean | True | Open snmpd ports on the linux firewall |
|  snmpd_template  | String | snmpd.conf.j2 | File name of snmpd.conf template |
|  snmpd_template_path | String | . | Path to snmpd_template file |


Dependencies
------------
None

Example Playbook
----------------

```yaml
- name: Install and configure snmpd
  hosts: all
  become: yes
  gather_facts: false
  vars:
    snmpd_v2_snmpcommunity: public
    snmpd_v2_poller: 
      - "192.168.1.0/24"

  roles:
     - { role: cblack34.snmpd }
```

Notes
-----


To Do
-----


License
-------

BSD

Author Information
------------------

Clayton Black   
cblack@claytontblack.com   
http://www.claytontblack.com